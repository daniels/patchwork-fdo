# Patchwork - automated patch tracking system
# Copyright (C) 2011 Jeremy Kerr <jk@ozlabs.org>
#
# This file is part of the Patchwork package.
#
# Patchwork is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Patchwork is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Patchwork; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from unittest.mock import MagicMock

from django.test import TestCase
from django.db.models import Q

from patchwork.tests.utils import defaults
from patchwork.views.api import TestResultFilter
from patchwork.models import TestState


class FilterQueryStringTest(TestCase):

    def testFilterQSEscaping(self):
        """test that filter fragments in a query string are properly escaped,
           and stray ampersands don't get reflected back in the filter
           links"""
        project = defaults.project
        defaults.project.save()
        url = '/project/%s/list/?submitter=a%%26b=c' % project.linkname
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'submitter=a&amp;b=c')
        self.assertNotContains(response, 'submitter=a&b=c')

    def testUTF8QSHandling(self):
        """test that non-ascii characters can be handled by the filter
           code"""
        project = defaults.project
        defaults.project.save()
        url = '/project/%s/list/?submitter=%%E2%%98%%83' % project.linkname
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class TestResultFilterTests(TestCase):
    def test_filter_project__using_id(self):
        qs = MagicMock()
        TestResultFilter().filter_project(qs, "", 42)

        q_objects = (Q(patch__project__pk=42) |
                     Q(revision__series__project__pk=42))
        qs.filter.assert_called_once_with(q_objects)

    def test_filter_project__using_linkname(self):
        qs = MagicMock()
        TestResultFilter().filter_project(qs, "", "project")

        q_objects = (Q(patch__project__linkname="project") |
                     Q(revision__series__project__linkname="project"))
        qs.filter.assert_called_once_with(q_objects)

    def test_filter_user__using_id(self):
        qs = MagicMock()
        TestResultFilter().filter_user(qs, "", 42)
        qs.filter.assert_called_once_with(user__pk=42)

    def test_filter_user__using_username(self):
        qs = MagicMock()
        TestResultFilter().filter_user(qs, "", "user")
        qs.filter.assert_called_once_with(user__username="user")

    def test_filter_state(self):
        qs = MagicMock()
        TestResultFilter().filter_state(qs, "", "pending,success,failure")

        qs.filter.assert_called_once()
        args, kwargs = qs.filter.call_args_list[0]
        self.assertEqual(len(args), 0, f"Expected no arguments, got {args}")
        self.assertEqual(len(kwargs), 1,
                         f"Expected one kwargs, got: {kwargs}")
        self.assertEqual(list(kwargs['state__in']), [TestState.STATE_PENDING,
                                                     TestState.STATE_SUCCESS,
                                                     TestState.STATE_FAILURE])

    def test_filter_series__null(self):
        qs = MagicMock()
        TestResultFilter().filter_series(qs, "", "null")
        qs.filter.assert_called_once_with(revision__series=None)

    def test_filter_series_by_id(self):
        qs = MagicMock()
        TestResultFilter().filter_series(qs, "", 42)
        qs.filter.assert_called_once_with(revision__series__pk=42)

    def test_filter_patch__null(self):
        qs = MagicMock()
        TestResultFilter().filter_patch(qs, "", "null")
        qs.filter.assert_called_once_with(patch=None)

    def test_filter_patch_by_id(self):
        qs = MagicMock()
        TestResultFilter().filter_patch(qs, "", 42)
        qs.filter.assert_called_once_with(patch__pk=42)
