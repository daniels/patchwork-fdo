class AccessControlAllowOriginMiddleware:
    """Allow all API GET (read-only) requests from any domain"""

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        if request.path.startswith('/api/') and \
           (request.method == 'GET' or request.method == 'OPTIONS'):
            response['Access-Control-Allow-Origin'] = '*'
            response['Access-Control-Allow-Headers'] = 'Content-Type'

        return response
