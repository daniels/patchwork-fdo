# The MIT License (MIT)
#
# Copyright (c) 2020 Arkadiusz Hiler
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from itertools import groupby


"""Testing migration logic is hard - you cannot call custom methods on model to
avoid breakages by changes in there. Containing all the data migration logic in
the migration file is also not an option - you can't import it and test it
easily.

To mitigate that issue this file is created where the data migration logic can
be contained and both tested and used in the migration file proper.
"""


def deduplicate_persons(person):
    persons = person.objects.all()
    persons = sorted(persons, key=lambda x: x.email.lower())
    persons = groupby(persons, key=lambda x: x.email.lower())

    duplicates = []

    for email, objs in persons:
        objs = list(objs)
        if len(objs) > 1:
            duplicates.append(objs)

    for dupes in duplicates:
        main = dupes.pop()

        for dupe in dupes:
            for comment in dupe.comment_set.all():
                comment.submitter = main
                comment.save()

            for patch in dupe.patch_set.all():
                patch.submitter = main
                patch.save()

            for series in dupe.series_set.all():
                series.submitter = main
                series.save()

            dupe = person.objects.get(pk=dupe.pk)
            dupe.delete()
