# Testing settings for Patchwork using the in-memory database.
from .base import *  # noqa

SECRET_KEY = '00000000000000000000000000000000000000000000000000'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    },
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
ENABLE_XMLRPC = True

SILENCED_SYSTEM_CHECKS = ['django_recaptcha.recaptcha_test_key_error']

# make tests faster
DEBUG = False
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)
