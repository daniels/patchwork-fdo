#!/bin/bash

apt-get -qq update
apt-get -qq install python3-virtualenv python3-pip tox
apt-get -qq install chromium chromium-driver nodejs npm
apt-get clean

npm install -g jshint jasmine-core karma karma-jasmine karma-chrome-launcher

pip install -r docs/requirements-dev-postgresql.txt

# tox builds venvs with broken pip versions - fix this:
virtualenv --upgrade-embed-wheels
