# Example production settings for Patchwork. Copy and edit this file
# to match your environment. For more information, see base.py and
# https://docs.djangoproject.com/en/3.2/ref/settings/
from .base import *  # noqa


def env_to_bool(key):
    val = os.environ.get(key)
    if val is None:
        return False
    else:
        return val.lower() in ['true', '1']


# Security
#
# You'll need to replace this to a random string. The following python code can
# be used to generate a secret key:
#
#      import string, random
#      chars = string.letters + string.digits + string.punctuation
#      print repr("".join([random.choice(chars) for i in range(0,50)]))

SECRET_KEY = os.environ['DJANGO_SECRET_KEY']

# Email
#
# Replace this with your own details

EMAIL_HOST = os.getenv('EMAIL_HOST', 'localhost')
EMAIL_PORT = os.getenv('EMAIL_PORT', 25)
EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER', '')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD', '')
EMAIL_USE_TLS = True

DEFAULT_FROM_EMAIL = os.getenv('DEFAULT_FROM_EMAIL',
    'Patchwork <patchwork@patchwork.example.com>')
SERVER_EMAIL = DEFAULT_FROM_EMAIL
NOTIFICATION_FROM_EMAIL = DEFAULT_FROM_EMAIL

ADMINS = (
    ('Karol Krol', 'karol.krol@intel.com'),
    ('Piotr Kira', 'piotr.kira@intel.com'),
)

MANAGERS = ADMINS

TIME_ZONE = 'UTC'

LANGUAGE_CODE = 'en-US'

ENABLE_XMLRPC = True

RECAPTCHA_PUBLIC_KEY = os.getenv('RECAPTCHA_PUBLIC_KEY', '')
RECAPTCHA_PRIVATE_KEY = os.getenv('RECAPTCHA_PRIVATE_KEY', '')

ALLOWED_HOSTS = [os.environ.get('HOST_URL')]
if env_to_bool('USE_FORWARDED_HEADERS'):
    USE_X_FORWARDED_HOST = True
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Database
#
# If you're using a postgres database, connecting over a local unix-domain
# socket, then the following setting should work for you. Otherwise,
# see https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DATABASE_NAME', ''),
        'USER': os.environ.get('DATABASE_USER', ''),
        'PASSWORD': os.environ.get('DATABASE_PASSWORD', ''),
        'HOST': os.environ.get('DATABASE_HOST', ''),
        'PORT': os.environ.get('DATABASE_PORT', ''),
    },
}

#
# Static files settings
# https://docs.djangoproject.com/en/3.2/ref/settings/#static-files
# https://docs.djangoproject.com/en/3.2/ref/contrib/staticfiles/#manifeststaticfilesstorage
#

STATIC_ROOT = os.environ.get('STATIC_ROOT', '/app/static')
STATICFILES_STORAGE = \
        'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'
